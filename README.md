# Weather App

First React JS project

Guide:

1. Git clone this project to your local
2. Open your terminal
3. CD to your project location
4. Run `npm i bootstrap` 
5. Cause we used open source weather icon,
   so Run `npm i https://github.com/erikflowers/weather-icons.git`
6. Save and you can view http://localhost:3000 in the browser


Deficiency:

1. Didn't set refresh interval for new tile independently


Future:
   I will continue to learn React and work on the project to make it perfect.


Update:
06/06/2020  Use css flex to make the tile can resiza automatically
