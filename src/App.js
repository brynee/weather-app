import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'weather-icons/css/weather-icons.css';
import Weatherlist from './app_component/Weatherlist'

function App() {

  return (
    <div className="App">
      <Weatherlist />
    </div>
  );
}

export default App;
