import React from 'react';


const Form = (props) => {
    return(
        <form onSubmit={props.getWeather}>
            <div className="form-row">
                <div className="col-6">
                    <input
                        className="form-control"
                        name="city"
                        type="text"
                        placeholder="Enter city name"
                    />
                </div>
                <div className="col-1.5">
                    <label className="control-label"> Refresh Interval: </label>
                </div>
                <div className="col">
                    <select className="form-control" name="interval">
                        <option value={10}>10s</option>
                        <option value={20}>20s</option>
                    </select>
                </div>

                <div className="col">
                    <button type="submit" className="btn btn-primary">Add</button>
                </div>
            </div>
        </form>
    )
}

export default Form;