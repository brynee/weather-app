import React from 'react';

export default ({ city, country, situation, temp, forecast_date,
    forecast_temp, forecast_condition, error, id, deleteWeather }) => {

    return (
            <div className="weather_box">
                {city && country && <h1>{city}, {country}</h1>}
                {error && <h1>{error}</h1>}
                {situation &&
                    <h5 className="py-4">
                        <i className={`wi ${matchValues(situation)} display-1`}></i>
                    </h5>
                }
                {temp && <h1 className="py-2">{temp}&deg;C</h1>}
                {situation && <h4 className="py-2">{situation}</h4>}

                {forecast_date && forecast_temp && forecast_condition &&
                    forecastCondition(forecast_date, forecast_temp, forecast_condition)
                }
                <button className="btn btn-danger" onClick={() => deleteWeather(id)}>Remove</button>
            </div>
    )
}

function forecastCondition(forecast_date, forecast_temp, forecast_condition){
    return(
        <h4>
            <span className="px-4">{forecast_date}</span>
            <span className="px-4">{forecast_temp}&deg;C</span>
            <span className="px-4">{forecast_condition}</span>
        </h4>
    );
}

function matchValues(situation){
    const conditions = {
        "Thunderstorm": "wi-thunderstorm",
        "Drizzle": "wi-sleet",
        "Rain": "wi-storm-showers",
        "Snow": "wi-snow",
        "Atmosphere": "wi-fog",
        "Clear": "wi-day-sunny",
        "Clouds": "wi-day-fog"
    }
    return(conditions[situation]);
}