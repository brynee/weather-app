import React, { useState, useRef, useEffect } from 'react';
import Weather from './Weather';
import Form from './Form';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'weather-icons/css/weather-icons.css';

const API_key = "5104b9b0aafe6d3ba00e131404897370"
const forecast_API_key = "3aac5b72e2f997bb298744eb1b18e77f"

const city_arr = []

const deleteWeather = (id, weathers, setWeathers) => {
    setWeathers(weathers.filter(weather => weather.id !== id))
    const weather = weathers.filter(weather => weather.id === id)
    console.log(weather)
    console.log(weather[0].city)
    const idx = city_arr.indexOf(weather[0].city.toLowerCase())
    if(idx>-1){
      city_arr.splice(idx, 1)
    }
}

export default () => {
  const [weathers, setWeathers] = useState([])

  const enterSameValue = () => {
    setWeathers([...weathers,
      {
        id: '',
        data: '',
        forecast_date: '',
        forecast_temp: '',
        forecast_condition: '',
        city: '',
        country: '',
        temp: '',
        situation: '',
        error: 'This city has been displayed'
      }])
  }

  let interval_refresh = useRef()

  async function fetchData(e) {
    e.preventDefault()
    const id = (weathers.length) ? weathers[weathers.length - 1].id + 1 : 0
    const city = e.target.elements.city.value
    const interval = e.target.elements.interval.value
    if (!city_arr.includes(city.toLowerCase())) {
      city_arr.push(city.toLowerCase())
      const forecast_data = await fetch(`https://api.openweathermap.org/data/2.5/forecast?q=${city}&APPID=${forecast_API_key}`)
        .then(res => res.json())
        .then(data => data)
      const api_data = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city}&APPID=${API_key}`)
        .then(res => res.json())
        .then(data => data)
      console.log(`city: ${city}, interval: ${interval}, apidata: ${api_data.name}`)

      if (city && interval && (typeof api_data.main != "undefined")) {
        const date = forecast_data.list[8]
        const date_array = forecast_data.list[8].dt_txt.split(' ')
        setWeathers([...weathers,
        {
          id: id,
          data: api_data,
          forecast_date: date_array[0],
          forecast_temp: Math.round(date.main.temp - 273.15),
          forecast_condition: date.weather[0].main,
          city: api_data.name,
          country: api_data.sys.country,
          temp: Math.round(api_data.main.temp - 273.15),
          situation: api_data.weather[0].main,
          error: ''
        }])
        // useEffect(() => {
        //   interval_refresh = setInterval(() => {
        //     const updateForecast = fetch(`https://api.openweathermap.org/data/2.5/forecast?q=${city}&APPID=${forecast_API_key}`)
        //     .then(res => res.json())
        //     .then(data => data)
        //     const updateCurrent = fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city}&APPID=${API_key}`)
        //     .then(res => res.json())
        //     .then(data => data)
        //     const date = updateForecast.list[8]
        //     const date_array = updateForecast.list[8].dt_txt.split(' ')
        //     setWeathers([...weathers,
        //       {
        //         id: id,
        //         data: updateCurrent,
        //         forecast_date: date_array[0],
        //         forecast_temp: Math.round(date.main.temp - 273.15),
        //         forecast_condition: date.weather[0].main,
        //         city: updateCurrent.name,
        //         country: updateCurrent.sys.country,
        //         temp: Math.round(updateCurrent.main.temp - 273.15),
        //         situation: updateCurrent.weather[0].main,
        //         error: ''
        //       }])
        //   }, interval*1000)
        //   return () => {
        //     clearInterval(interval_refresh.current)
        //   }
        // })
      }
      else {
        setWeathers([...weathers,
        {
          id: '',
          data: '',
          forecast_date: '',
          forecast_temp: '',
          forecast_condition: '',
          city: '',
          country: '',
          temp: '',
          situation: '',
          error: 'Please enter a valid city'
        }])
      }
    } else {
      enterSameValue()
    }
  }

  return (
    <div className="container">
      <h1>Weather Hook</h1>
      <Form getWeather={fetchData} />
      <hr />
      <div className="weather_container">
        {weathers.map(weather => (
          <Weather city={weather.city} country={weather.country} situation={weather.situation} temp={weather.temp}
            forecast_date={weather.forecast_date} forecast_temp={weather.forecast_temp} forecast_condition=
            {weather.forecast_condition} error={weather.error} id={weather.id} deleteWeather={(id) =>
              deleteWeather(id, weathers, setWeathers)} />
        ))}
      </div>
    </div>
  )
}